CONFIG set sysvshm=new;
CONFIG set sysvsem=new;
CONFIG set allow.sysvipc=1;
CONFIG set allow.raw_sockets=1;

# No special characters
ARG ZABBIXVERSION="7"
ARG ZABBIXDBPASSWORD="$(cat /root/${JAIL_NAME}_db_password.txt || head -c 26 /dev/urandom | base64 | tr -dc 'a-zA-Z0-9')"
ARG ZABBIX_SERVER_IP="127.0.0.1"
ARG TIMEZONE_CONTINENT="America"
ARG TIMEZONE_CITY="Toronto"
ARG CERT_COUNTRY="CA"
ARG CERT_STATE_PROVINCE="ON"
ARG CERT_CITY="TORONTO"
ARG CERT_ORG_NAME="Zabbix"
ARG CERT_ORG_UNIT="ITDepartment"
ARG CERT_COMMON_NAME="zabbix.local"

# Store Passwords
CMD echo "${ZABBIXDBPASSWORD}" > /root/${JAIL_NAME}_db_password.txt

#Install the required packages
PKG nginx mysql80-server
PKG zabbix${ZABBIXVERSION}-server zabbix${ZABBIXVERSION}-frontend-php83 zabbix${ZABBIXVERSION}-agent
PKG php83 php83-mysqli php83-mbstring php83-gd php83-bcmath php83-curl


#Move the config files into /usr of the jail
CP usr /
CP zfs-discovery.sh /usr/local/etc/zabbix${ZABBIXVERSION}/scripts
CP zpool-discovery.sh /usr/local/etc/zabbix${ZABBIXVERSION}/scripts

# Create self signed SSL Certs
CMD openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /usr/local/etc/nginx/zabbix.key -out /usr/local/etc/nginx/zabbix.crt -subj "/C=${CERT_COUNTRY}/ST=${CERT_STATE_PROVINCE}/L=${CERT_CITY}/O=${CERT_ORG_NAME}/OU='${CERT_ORG_UNIT}'/CN=${CERT_COMMON_NAME}"

#Modify the zabbix config
CMD sed -i '' -E "s/^(# )?ExternalScripts=.*$/ExternalScripts=\/usr\/local\/etc\/zabbix${ZABBIXVERSION}\/scripts/g" /usr/local/etc/zabbix${ZABBIXVERSION}/zabbix_server.conf 
CMD sed -i '' -E "s/^(# )?DBPassword=.*$/DBPassword=$(cat /root/${JAIL_NAME}_db_password.txt)/g" /usr/local/etc/zabbix${ZABBIXVERSION}/zabbix_server.conf 
CMD sed -i '' -E "s/^ServerActive=.*$/ServerActive=${ZABBIX_SERVER_IP}/g" /usr/local/etc/zabbix${ZABBIXVERSION}/zabbix_agentd.conf 
CMD sed -i '' -E "s/^Server=.*$/Server=${ZABBIX_SERVER_IP}/g" /usr/local/etc/zabbix${ZABBIXVERSION}/zabbix_agentd.conf 
CMD sed -i '' -E "s/^php_value\[date\.timezone\].*$/php_value\[date\.timezone\] = ${TIMEZONE_CONTINENT}\/${TIMEZONE_CITY}/g" /usr/local/etc/php-fpm.d/www.conf

# Set permissions
CMD chown -R www /usr/local/www/zabbix${ZABBIXVERSION}

#Enable services on boot
SYSRC mysql_enable=YES
SYSRC zabbix_server_enable=YES
SYSRC zabbix_agentd_enable=YES
SYSRC php_fpm_enable=YES
SYSRC nginx_enable=YES

#Start all services
SERVICE mysql-server start
SERVICE zabbix_server start
SERVICE zabbix_agentd start
SERVICE php_fpm start
SERVICE nginx start

#Setup the database for Zabbix
# CMD mysql -e "install component 'file://component_validate_password';"
CMD mysql -e "create database zabbix character set utf8mb4 collate utf8mb4_bin;" || echo "Database already found"
CMD mysql -e "create user zabbix@'localhost' identified by '$(cat /root/${JAIL_NAME}_db_password.txt)';" || mysql -e "alter user zabbix@'localhost' identified by '$(cat /root/${JAIL_NAME}_db_password.txt)';"
CMD mysql -e "grant all privileges on zabbix.* to zabbix@'localhost';"
CMD mysql -e "set global log_bin_trust_function_creators = 1;"
CMD mysql --user=zabbix --password="$(cat /root/${JAIL_NAME}_db_password.txt)" zabbix < /usr/local/share/zabbix${ZABBIXVERSION}/server/database/mysql/schema.sql || echo "Schema already exists"
CMD mysql --user=zabbix --password="$(cat /root/${JAIL_NAME}_db_password.txt)" zabbix  < /usr/local/share/zabbix${ZABBIXVERSION}/server/database/mysql/images.sql || echo "Images already exists"
CMD mysql --user=zabbix --password="$(cat /root/${JAIL_NAME}_db_password.txt)" zabbix  < /usr/local/share/zabbix${ZABBIXVERSION}/server/database/mysql/data.sql || echo "Data already exists"
CMD mysql -e "set global log_bin_trust_function_creators = 0;"

#Print information to user
CMD echo "Your system has been setup. Please access the web interface and change the default username and password."
CMD echo "Web Interface (Username: Admin, Password: zabbix)"
CMD echo "Database (Username: zabbix, Password: ${ZABBIXDBPASSWORD})"
CMD echo "Database Password saved in: /root/${JAIL_NAME}_db_password.txt"
CMD echo "Please restart first for config changes to apply to jail."